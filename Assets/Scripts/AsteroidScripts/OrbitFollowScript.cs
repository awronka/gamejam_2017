﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitFollowScript : MonoBehaviour {

	public GameObject followOrbit;
	public float orbitSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (followOrbit.transform.position, new Vector3 (0, 0, 1), orbitSpeed);
	}
}
