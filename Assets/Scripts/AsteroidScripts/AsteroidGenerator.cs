﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour {

	public float AsteroidGenerationRate;
	private float nextAsteroid;
	public GameObject topPoint;
	public GameObject bottomPoint;
	private Vector3 generationPoint;
	public float startSpeed;

	public Sprite[] asteroidSprites;

	public ObjectPooler astroidPool;

	private GameManager gm;
	private LevelManager lm;


	// Use this for initialization
	void Start () {
		nextAsteroid = Time.time + AsteroidGenerationRate;
		gm = FindObjectOfType<GameManager> ();
		lm = FindObjectOfType<LevelManager> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Time.time > nextAsteroid && gm.gameActive && !lm.betweenLevels) {
			float randomVal = Random.Range (topPoint.transform.position.y, bottomPoint.transform.position.y);
			generationPoint = new Vector3 (transform.position.x, randomVal, transform.position.z);
			GameObject astroid = (GameObject)astroidPool.GetPooledObject ();
			astroid.SetActive (true);
			astroid.transform.position = generationPoint;
			astroid.transform.rotation = transform.rotation;
			astroid.GetComponent<AstroidController> ().hasBeenWaved = false;
			astroid.GetComponent<AstroidController> ().isInOrbit = false;
			astroid.GetComponent<AstroidController> ().astroidSpeed =startSpeed;
			astroid.GetComponent<SpriteRenderer> ().sprite = asteroidSprites [Random.Range (0, 6)];



			nextAsteroid = Time.time + AsteroidGenerationRate;
		}
	}
}
