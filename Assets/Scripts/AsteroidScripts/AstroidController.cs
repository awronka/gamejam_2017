﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidController : MonoBehaviour {

	public float astroidSpeed;
	private Vector2 astroidVelocity;
	private Rigidbody2D astroidRB;
	public LayerMask waveLayer;
	public LayerMask orbitLayer;
	public int asteroidScore;

	public GameObject explosion;

	private float rotationAngle;

	private GameObject rotateOrbit = null;

	public bool hasBeenWaved;

	public bool isInOrbit;

	private GameManager gm;



	// Use this for initialization
	void Start () {
		rotationAngle = Random.Range (-4, 4);
		astroidRB = GetComponent<Rigidbody2D> ();
		gm = FindObjectOfType<GameManager> ();

	}
		

	// Update is called once per frame
	void Update () {

		transform.Rotate (Vector3.forward * rotationAngle);

		if (!isInOrbit) {
			astroidRB.velocity = new Vector2 (astroidSpeed, -astroidSpeed);
		}

		if (isInOrbit && rotateOrbit != null) {
			transform.RotateAround (rotateOrbit.transform.position, new Vector3 (0, 0, 1), 10 * Time.deltaTime);
		}
	}

	void OnTriggerStay2D(Collider2D other){
		if (other.gameObject.layer == Mathf.Log(waveLayer.value,2)) {
			astroidSpeed = astroidSpeed/2;
			hasBeenWaved = true;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.layer == Mathf.Log (waveLayer.value, 2)) {
			astroidSpeed = astroidSpeed / 2;
			hasBeenWaved = true;
		}else {
//			GrabOrbit (other);
		}
	}

	void OnTriggerExit2D(Collider2D other){
//		GrabOrbit (other);
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "planet") {
			if (other.gameObject.name == "Jovian") {
				gm.hits[2] += 1;
			} else if (other.gameObject.name == "Earth") {
				gm.hits[1] += 1;
			} else if (other.gameObject.name == "Desert") {
				gm.hits[0] += 1;
			}
		} 

		if (other.gameObject.name == "Sun") {
			Instantiate (explosion, transform.position, transform.rotation);
			gm.boom.Play ();
			gameObject.SetActive (false);
			gm.score += 100;
		}
	}

	void GrabOrbit (Collider2D other){
//		if (other.gameObject.layer == Mathf.Log (orbitLayer.value, 2) && hasBeenWaved) {
//			rotateOrbit = other.gameObject;
//			hasBeenWaved = false;
//			astroidSpeed = 0;
//			isInOrbit = true;
//		}
	}


}
