﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public int currentLevel = 0;
	public int maxLvl;
	public float roundIntervalTime;
	private float timeRemaining;
	public int[] maxScoreByLvl;
	public float[] asteroidGenerationByLvl;

	private GameManager gm;
	private AsteroidGenerator ag;

	public Text maxScoreText;
	public Text goalMessageText;
	public Text levelText;
	public GameObject LevelIntervalCanvas;

	public bool betweenLevels;
	public bool waiting;

	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager> ();
		ag = FindObjectOfType<AsteroidGenerator> ();
		maxScoreText.text = "Goal Score : " + maxScoreByLvl [currentLevel] + gm.score;
		levelText.text = "Current Level : " + (currentLevel + 1);
		timeRemaining = roundIntervalTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (gm.score >= maxScoreByLvl [currentLevel] && betweenLevels == false && currentLevel != maxLvl) {
			betweenLevels = true;
		}

		if (betweenLevels == true && waiting == false) {
			waiting = true;
			currentLevel++;
			maxScoreText.text = "Goal : " + (maxScoreByLvl [currentLevel] + gm.score);
			levelText.text = "Current Level : " + (currentLevel + 1);
			ag.AsteroidGenerationRate = asteroidGenerationByLvl [currentLevel];
			LevelIntervalCanvas.SetActive (true);
			goalMessageText.text = "You need to get " + maxScoreByLvl [currentLevel] + " more this round!";
			DeactivateAsteroid ();


		}
		if (betweenLevels == true) {
			timeRemaining -= Time.deltaTime;
		}

		if (timeRemaining <= 0) {
			timeRemaining = roundIntervalTime;
			betweenLevels = false;
			waiting = false;
			LevelIntervalCanvas.SetActive (false);
		}

	}

	public void ResetGame(){
		currentLevel = 0;
		levelText.text = "Current Level : " + (currentLevel + 1);
		maxScoreText.text = "Goal Score : " + maxScoreByLvl [currentLevel];
		ag.AsteroidGenerationRate = asteroidGenerationByLvl [currentLevel];
		DeactivateAsteroid ();


	}

	public void DeactivateAsteroid(){
		GameObject[] asteroids = GameObject.FindGameObjectsWithTag ("Asteroid");
		for (int i = 0; i < asteroids.Length; i++) {
			asteroids[i].SetActive(false);
		}
	}
}
