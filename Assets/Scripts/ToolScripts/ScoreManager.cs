﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public Text scoreText;
	private GameManager gm;

	void Start(){
		gm = GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "SCORE : " + gm.score;
	}
}
