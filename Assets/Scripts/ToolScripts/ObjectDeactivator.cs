﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDeactivator : MonoBehaviour {


	void OnCollisionEnter2D(Collision2D other){
		if (other.transform.name == "ObjectDeactivator") {
			gameObject.SetActive (false);
		}
	}
}
