﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public int earthHits;
	public int desertHits;
	public int jovianHits;

	public int[] hits;

	public GameObject earth;
	public GameObject desert;
	public GameObject jovian;

	private int ScoreToWin;
	public int score;

	public GameObject[] planetsArray;

	public GameObject[] startPositions;

	public bool gameActive;
	public bool gameOver;




	public GameObject scoreCanvas;
	public GameObject menuCanvas;
	public GameObject gameOverCanvas;
	public GameObject intervalText;
	public Text gameOverText;
	public Text menuText;

	private LevelManager lm;

	public GameObject explosion;

	public AudioSource boom;

	public GameObject waivImage;
	public float titleCardLife;

	// Use this for initialization
	void Start () {
		score = 0;
		lm = GetComponent<LevelManager> ();
		gameActive = false;
		menuCanvas.SetActive (true);
		scoreCanvas.SetActive (false);
		ScoreToWin = lm.maxScoreByLvl [lm.maxScoreByLvl.Length - 1];
		menuText.text = "Controls : left arrow : rotate Left right arrow: rotate right: space bar to emit gravity waves. " +
			"Score " + ScoreToWin + " points to win!";
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > titleCardLife) {
			waivImage.SetActive (false);
		}

		trackHits ();
		checkForGameState ();
		checkForGameOver ();
	}

	void checkForGameOver(){
		if (score >= ScoreToWin && !gameOver) {
			gameOver = true;
			gameActive = false;
			menuCanvas.SetActive (false);
			scoreCanvas.SetActive (false);
			gameOverCanvas.SetActive (true);
			gameOverText.text = "You beat the game with a score of " + ScoreToWin + "! Press esc to Continue...";
		} else if (!earth.activeSelf && !desert.activeSelf && !jovian.activeSelf && !gameOver){
			gameOver = true;
			gameActive = false;
			menuCanvas.SetActive (false);
			scoreCanvas.SetActive (false);
			gameOverCanvas.SetActive (true);
			gameOverText.text = "Game Over! Your Solar System is Destroyed! Press esc to Continue...";
		}
	}

	void checkForGameState(){
		if (Input.GetKeyUp (KeyCode.Return) && !gameActive && !gameOver) {
			initializeGame (planetsArray, startPositions);
		} else if (gameActive && Input.GetKeyUp (KeyCode.Escape) && !gameOver) {
			menuCanvas.SetActive (true);
			scoreCanvas.SetActive (false);
			gameOverCanvas.SetActive (false);
			intervalText.SetActive (false);
			gameActive = false;
		} else if (Input.GetKeyUp (KeyCode.Escape) && !gameActive && gameOver) {
			menuCanvas.SetActive (true);
			scoreCanvas.SetActive (false);
			gameOverCanvas.SetActive (false);
			intervalText.SetActive (false);
			GameObject[] asteroids = GameObject.FindGameObjectsWithTag ("Asteroid");
			for (int i = 0; i < asteroids.Length; i++) {
				asteroids[i].SetActive(false);
			}
			gameOver = false;
			score = 0;
			for (int i = 0; i < planetsArray.Length; i++) {
				hits [i] = 0;
				initializePlanets (planetsArray [i], startPositions [i]);
			}
		} else if (Input.GetKeyUp (KeyCode.Q)) {
			Application.Quit ();
		}
	}

	void initializeGame(GameObject[] planets, GameObject[] planetsPoints){
		for (int i = 0; i < planets.Length; i++) {
			hits [i] = 0;
			initializePlanets (planets [i], planetsPoints [i]);
		}
		gameActive = true;
		score = 0;
		scoreCanvas.SetActive (true);
		menuCanvas.SetActive (false);
		intervalText.SetActive (false);
		lm.ResetGame ();

	}

	void initializePlanets(GameObject planet, GameObject planetPoint){
		if (!planet.activeSelf) {
			planet.SetActive (true);
		}
		planet.transform.position = planetPoint.transform.position;
		planet.transform.rotation = planetPoint.transform.rotation;
	}

	void setGameOverMessage(string msg){
		
	}



	void trackHits (){
		if (hits [0] >= desertHits && desert.activeSelf) {
			Instantiate (explosion,desert.transform.position,desert.transform.rotation);
			boom.Play ();
			desert.SetActive (false);

		}

		if (hits [1] >= earthHits && earth.activeSelf) {
			Instantiate (explosion,earth.transform.position,earth.transform.rotation);
			boom.Play ();
			earth.SetActive (false);
		}

		if (hits [2] >= jovianHits && jovian.activeSelf) {
			Instantiate (explosion,jovian.transform.position,jovian.transform.rotation);
			boom.Play ();
			jovian.SetActive (false);
		}
	}
}
