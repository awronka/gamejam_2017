﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToDie : MonoBehaviour {


	public float TimeToDestroy;

	
	// Update is called once per frame
	void Update () {
		Destroy (gameObject, TimeToDestroy);
	}
}
