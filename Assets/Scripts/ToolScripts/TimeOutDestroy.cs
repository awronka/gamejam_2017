﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeOutDestroy : MonoBehaviour {

	public float lifeTime;
	private float timeToDie;

	void Start(){
		timeToDie = 0f;
	}

	// Update is called once per frame
	void Update () {
		timeToDie += Time.deltaTime;
		if (timeToDie >= lifeTime) {
			timeToDie = 0f;
			gameObject.SetActive (false);
		}
	}
}
