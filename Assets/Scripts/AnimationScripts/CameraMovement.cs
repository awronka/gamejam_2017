﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
	public float xLimit;
	public float yLimit;
	public float movementSpeedX;
	public float movementSpeedY;
	private float randomLimitX;
	private float randomLimitY;

	// Use this for initialization
	void Start () {
		randomLimitX = Random.Range (0, xLimit);
		randomLimitY = Random.Range (0, yLimit);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (transform.position.x + movementSpeedX, transform.position.y + movementSpeedY, transform.position.z);

		if (transform.position.x > randomLimitX) {
			movementSpeedX = movementSpeedX * -1;
			randomLimitX = Random.Range (0, xLimit);
		} else if (transform.position.x < -randomLimitX) {
			movementSpeedX = movementSpeedX * -1;
			randomLimitX = Random.Range (0, xLimit);

		}

		if (transform.position.y > randomLimitY) {
			movementSpeedY = movementSpeedY * -1;
			randomLimitY = Random.Range (0, yLimit);
		} else if (transform.position.y < -randomLimitY) {
			movementSpeedY = movementSpeedY * -1;
			randomLimitY = Random.Range (0, yLimit);
		}

	}
}
