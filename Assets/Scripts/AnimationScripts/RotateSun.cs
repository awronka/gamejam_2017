﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSun : MonoBehaviour {

	public float rotatationSpeed;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		Rotate ();
	}

	void Rotate () {
		transform.RotateAround (transform.position, new Vector3 (0, 0, 1), rotatationSpeed);
	}
}
