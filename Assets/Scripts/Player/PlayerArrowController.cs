﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArrowController : MonoBehaviour {

	public float rotationSpeed =5f;
	public GameObject parent;

	public float waveInterval = 2f;

	public ObjectPooler wavePool;
	public GameObject waveGenerationPoint;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.RotateAround (parent.transform.position, new Vector3 (0, 0, 1), rotationSpeed * Time.deltaTime);
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			transform.RotateAround (parent.transform.position, new Vector3 (0, 0, 1), -rotationSpeed * Time.deltaTime);
		}

//		if (Input.GetKey (KeyCode.Space) && (Time.time >= nextWave)) {
//			GameObject wave = (GameObject)wavePool.GetPooledObject ();
//			wave.SetActive (true);
//			wave.transform.position = waveGenerationPoint.transform.position;
//
//			wave.transform.rotation = transform.rotation;
//			nextWave = waveInterval + Time.time;
//
//		}
	}
}
