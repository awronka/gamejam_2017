﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SolarFlareGenerator : MonoBehaviour {

	public bool isFlaring;

	public GameObject solarFlare;

	public AudioSource solarFlareSound;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.F)) {
			if (!isFlaring) {
				GameObject go = (GameObject)Instantiate (solarFlare, transform.position, transform.rotation);
				solarFlareSound.Play ();
				DestroyObject (go, 1f);
			}
		}
	}

	void SetInitialReferences(){
	
	}
}


