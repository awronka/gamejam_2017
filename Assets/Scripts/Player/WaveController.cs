﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour {

	public float speed;
	public GameObject PlaceToGo;
	public GameObject PlaceToRetreat;


	// Use this for initialization
	void Start () {
		PlaceToGo = GameObject.Find ("WaveTravelPoint");
		PlaceToRetreat = GameObject.Find ("MinWavePoint");
	}
		
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			transform.position = Vector3.MoveTowards (transform.position, PlaceToGo.transform.position, speed * Time.deltaTime);
		}
		else {
			transform.position = Vector3.MoveTowards(transform.position, PlaceToRetreat.transform.position, (speed/2) * Time.deltaTime);
		}

		if (transform.localPosition.y > PlaceToGo.transform.localPosition.y) {
			Destroy (gameObject);
		}

		if (transform.localPosition.y < -0.25) {
			Destroy (gameObject);
		}

	}
}
