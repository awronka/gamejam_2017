﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveFabricator : MonoBehaviour {
	public float growthRate;
	public float shrinkRate;
	public GameObject growthObject;
	private float minSize;
	private float minOffset;

	public float waveForcePush;
	public float waveForcePull;

	public float TimeToPull;
	private float remainingTimeToPull;


	public GameObject maxPoint;
	public GameObject travelExpandPoint;
	public GameObject placeToDestroy;

	public GameObject pullPushPoint;

	public GameObject waveChild;


	private BoxCollider2D myBXC;

	private float waveInterval;
	public float waveIntervalTop;
	public float waveIntervalBottom;

	private float scaleVal;
	public float scaleValueTop;
	public float scaleValueBottom;

	private float nextWave = 0;

	public AudioSource grav;


	// Use this for initialization
	void Start () {
		myBXC = GetComponent<BoxCollider2D> ();
	}

	// Update is called once per frame
	void Update () {
		Transform gt = growthObject.transform;
		Transform te = travelExpandPoint.transform;
		float growthSpeed = Time.deltaTime;
		if (Input.GetKey (KeyCode.Space) && te.localPosition.y <= maxPoint.transform.localPosition.y) {
			if (!grav.isPlaying) {
				grav.Play ();
			}

			myBXC.offset = new Vector2 (myBXC.offset.x, myBXC.offset.y + growthRate * growthSpeed);
			myBXC.size = new Vector2 (myBXC.size.x, myBXC.size.y + growthRate * growthSpeed * 2);
			gt.localPosition = new Vector3 (gt.localPosition.x, gt.localPosition.y + growthRate * growthSpeed, gt.localPosition.z);
			gt.localScale = new Vector3 (gt.localScale.x, gt.localScale.y + growthRate * growthSpeed * 2, gt.localScale.z);
			te.localPosition = new Vector3 (te.localPosition.x, te.localPosition.y + growthRate * growthSpeed * 2, te.localPosition.z);

			if (Time.time > nextWave) {
				GameObject wv = (GameObject)Instantiate (waveChild, placeToDestroy.transform.position, placeToDestroy.transform.rotation);
				scaleVal = Random.Range (scaleValueBottom, scaleValueTop);
				if (wv != null) {
					wv.transform.localScale = new Vector3(scaleVal,scaleVal,scaleVal);
					wv.transform.parent = this.transform;
				}
				waveInterval = Random.Range (waveIntervalBottom, waveIntervalTop);
				nextWave = Time.time + waveInterval;
			}

		
		} else if (myBXC.offset.y > 0) {
			if (grav.isPlaying) {
				grav.Stop ();
			}
			myBXC.offset = new Vector2 (myBXC.offset.x, myBXC.offset.y - shrinkRate * growthSpeed);
			myBXC.size = new Vector2 (myBXC.size.x, myBXC.size.y - shrinkRate * growthSpeed * 2);
			gt.localPosition = new Vector3 (gt.localPosition.x, gt.localPosition.y - shrinkRate * growthSpeed, gt.localPosition.z);
			gt.localScale = new Vector3 (gt.localScale.x, gt.localScale.y - shrinkRate * growthSpeed * 2, gt.localScale.z);
			te.localPosition = new Vector3 (te.localPosition.x, te.localPosition.y - shrinkRate * growthSpeed * 2 , te.localPosition.z);
		}


	}

	void OnTriggerStay2D(Collider2D coll) {
		if (coll.transform.tag == "Asteroid") {
			if (Input.GetKey (KeyCode.Space)) {
				remainingTimeToPull = TimeToPull;
				//coll.transform.position = Vector3.MoveTowards (coll.transform.position, pullPushPoint.transform.position, -Time.deltaTime * waveForcePull);
			} else if (remainingTimeToPull > 0) {
				coll.transform.position = Vector3.MoveTowards (coll.transform.position, pullPushPoint.transform.position, Time.deltaTime * waveForcePull);
				remainingTimeToPull -= Time.deltaTime;
			}
		}
	}
}
