﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SolarFlareController : MonoBehaviour {

	public int power;
	public float width;
	public float length;
	public bool isFlaring;

	public GameObject explosion;

	private GameManager gm;



	// Use this for initialization
	void Start () {
		SetInitialReferences ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void SetInitialReferences(){
		gm = FindObjectOfType<GameManager> ();

	}

	void OnCollisionEnter2D(Collision2D other){
		Debug.Log ("hit asteroid outside");
		if (other.transform.tag == "Asteroid") {
			Debug.Log ("hit asteroid");
			Instantiate (explosion, other.transform.position, other.transform.rotation);
			gm.boom.Play ();
			gm.score += 100;
			other.gameObject.SetActive (false);
		}
	}
}


